import express from 'express';

const router = express.Router();

router.get('/api/users/currentuser', (req, res) => {
    res.send('Current user is Asad Hanif...');
});

export { router as currentUserRouter };
